package com.example.fckk18355.halodoc.view;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.fckk18355.halodoc.R;
import com.example.fckk18355.halodoc.view.presenter.NewsPresenter;
import com.example.fckk18355.halodoc.view.presenter.NewsPresenterImpl;


public class MainActivity extends AppCompatActivity implements com.example.fckk18355.halodoc.view.View {

    private RecyclerView mNewsRecyclerView;
    private ProgressBar mProgressBar;
    private NewsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressBar = findViewById(R.id.progress_bar);
        mNewsRecyclerView = findViewById(R.id.news_recycler_view);
        mPresenter = new NewsPresenterImpl(this);
        mNewsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mPresenter.fetchData("");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewAndroidActionBar.clearFocus();
                mPresenter.fetchData(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void showNextView(String data) {
        Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
        intent.putExtra(WebViewActivity.URL, data);
        startActivity(intent);
    }

    @Override
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorToast(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void update() {
        DataAdapter adapter = new DataAdapter(mPresenter.getNewsData(), new DataAdapter.ItemClickListener() {
            @Override
            public void onNewsItemClicked(int pos, String url) {
                if (mProgressBar.getVisibility() != View.VISIBLE) {
                    mPresenter.onNewsItemClicked(pos);
                }
            }
        });

        mNewsRecyclerView.setAdapter(adapter);
    }
}
