package com.example.fckk18355.halodoc.view;

/**
 * Created by fckk18355 on 13/10/17.
 */
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fckk18355.halodoc.R;
import com.example.fckk18355.halodoc.model.Data;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private final ItemClickListener mClickListener;
    private List<Data.Hits> mDataList;

    public DataAdapter(List<Data.Hits> arrayList, ItemClickListener clickListener) {
        mDataList = arrayList;
        mClickListener = clickListener;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.new_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.mTitleTextView.setText(mDataList.get(i).title);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onNewsItemClicked(i, mDataList.get(i).url);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public interface ItemClickListener{
        public void onNewsItemClicked(int pos, String url);
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView mTitleTextView;
        public ViewHolder(View view) {
            super(view);
            mTitleTextView = (TextView)view.findViewById(R.id.title);

        }
    }

}

