package com.example.fckk18355.halodoc.view;

/**
 * Created by fckk18355 on 13/10/17.
 */

public interface View {

    public void showNextView(String data);

    public void showProgressBar();

    public void hideProgressBar();

    public void showErrorToast(String error);

    public void update();
}
