package com.example.fckk18355.halodoc.view.presenter;

import android.text.TextUtils;

import com.example.fckk18355.halodoc.model.Data;
import com.example.fckk18355.halodoc.network.RetrofitManager;
import com.example.fckk18355.halodoc.view.View;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fckk18355 on 13/10/17.
 */

public class NewsPresenterImpl implements NewsPresenter {

    private View mView;
    private List<Data.Hits> mNewsData;


    public NewsPresenterImpl(View view) {
        mView = view;
    }

    // fetch data from server
    @Override
    public void fetchData(String query) {
        mView.showProgressBar();
        RetrofitManager.getInstance().getHaloDocService().getNews(query).enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                mView.hideProgressBar();
                if (response.body() != null) {
                    mNewsData = filterNullData(response.body().hits);
                    if (mNewsData.size() > 0) {
                        mView.update();
                    }else{
                        mView.showErrorToast("No data available on server");
                    }
                } else {
                    mView.showErrorToast("Something went wrong");
                }
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
                mView.hideProgressBar();
                mView.showErrorToast("Something went wrong");
            }
        });
    }


    /// Filter nulls from data list
    private List<Data.Hits> filterNullData(List<Data.Hits> hitsList) {
        List<Data.Hits> result = new ArrayList<>();
        if (hitsList != null){
            for (Data.Hits hits : hitsList) {
                if (TextUtils.isEmpty(hits.title) || TextUtils.isEmpty(hits.url)) {
                    continue;
                }
                result.add(hits);
            }
        }
        return result;
    }

    @Override
    public void onNewsItemClicked(int pos) {
        mView.showNextView(mNewsData.get(pos).url);
    }

    @Override
    public List<Data.Hits> getNewsData() {
        return mNewsData;
    }

}
