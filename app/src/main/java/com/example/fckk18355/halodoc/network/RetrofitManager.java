package com.example.fckk18355.halodoc.network;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by fckk18355 on 13/10/17.
 */
/// Singleton class provide Retrofit client for networking.
public class RetrofitManager {

    private HaloDocService mHaloDocService;
    private static RetrofitManager sInstance;

    // Get instance.
    public static RetrofitManager getInstance(){
        if (sInstance == null)
            sInstance = new RetrofitManager();

        return sInstance;
    }


    // private constructor
    private RetrofitManager(){

        String API_BASE_URL = "https://hn.algolia.com/api/v1/";
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );

        Retrofit retrofit = builder.client(httpClient.build()).build();

        mHaloDocService = retrofit.create(HaloDocService.class);
    }

    // get Halodoc service.
    public HaloDocService getHaloDocService(){
        return mHaloDocService;
    }
}
