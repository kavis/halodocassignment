package com.example.fckk18355.halodoc.view.presenter;

import com.example.fckk18355.halodoc.model.Data;

import java.util.List;

/**
 * Created by fckk18355 on 13/10/17.
 */

public interface NewsPresenter  {

    public void fetchData(String query);
    public void onNewsItemClicked(int pos);
    public List<Data.Hits> getNewsData();
}
