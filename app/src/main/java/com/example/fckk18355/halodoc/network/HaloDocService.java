package com.example.fckk18355.halodoc.network;

import com.example.fckk18355.halodoc.model.Data;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by fckk18355 on 13/10/17.
 */

public interface HaloDocService {

    @GET("search")
    Call<Data> getNews(@Query("query") String query);
}
